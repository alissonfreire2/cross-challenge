defmodule CrossChallenge.Repo.Migrations.ChangePageColumnsTypeInInfoTable do
  use Ecto.Migration

  def change do
    alter table(:info) do
      remove :success_pages
      remove :fetched_pages
      remove :failed_pages

      add :success_pages, {:array, :integer}, default: []
      add :fetched_pages, {:array, :integer}, default: []
      add :failed_pages, {:array, :integer}, default: []
    end
  end
end
