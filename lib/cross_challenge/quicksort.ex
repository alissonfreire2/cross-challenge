defmodule CrossChallenge.QuickSort do
  @moduledoc """
      O Quick utiliza a abordagem dividir para conquistar

      Que basicamente consiste em escolher um elemento pivô, que nessa abordagem será a cabeça da lista
      Depois separar a lista da entrada em outras duas listas
      a da esquerda com os items menores que o pivô
      e da direita contendo os números maiores que o pivô
    """

  # Casos base
  def sort([]), do: []
  def sort([pivot | []]), do: [pivot]

  def sort([pivot | tail]) do
    left = for i <- tail, i < pivot, do: i
    right = for j <- tail, j > pivot, do: j

    sort(left) ++ [pivot] ++ sort(right)
  end
end
