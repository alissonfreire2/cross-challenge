defmodule CrossChallenge.SortTest do
  use CrossChallenge.DataCase
  alias CrossChallenge.{QuickSort, MergeSort}

  test "check merge sort function" do

    unsorted_numbers = for _ <- 0..10, do: :rand.uniform()

    assert MergeSort.sort(unsorted_numbers) == Enum.sort(unsorted_numbers)
    assert MergeSort.sort([1]) == Enum.sort([1])
    assert MergeSort.sort([]) == Enum.sort([])
    assert MergeSort.sort([9,8,7,6,5,4,3,2,1]) == Enum.sort([9,8,7,6,5,4,3,2,1])
  end

  test "check quick sort function" do

    unsorted_numbers = for _ <- 0..10, do: :rand.uniform()

    assert QuickSort.sort(unsorted_numbers) == Enum.sort(unsorted_numbers)
    assert QuickSort.sort([1]) == Enum.sort([1])
    assert QuickSort.sort([]) == Enum.sort([])
    assert QuickSort.sort([9,8,7,6,5,4,3,2,1]) == Enum.sort([9,8,7,6,5,4,3,2,1])
  end
end
