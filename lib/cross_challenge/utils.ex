defmodule CrossChallenge.Utils do
  alias Ecto.Changeset

  def is_datetime?(%DateTime{} = _date) do
    true
  end

  def is_datetime?(date) when is_bitstring(date) do
    case DateTime.from_iso8601(date) do
      {:ok, _, _} -> true
      _ -> false
    end
  end

  def is_datetime?(_date) do
    false
  end

  def validate_integer(changeset, field) do
    if is_integer(Changeset.get_field(changeset, field)) do
      changeset
    else
      Changeset.add_error(changeset, field, "An integer is expected.")
    end
  end

  def paginator_to_map(paginator) do
    data =
      Enum.map(
        paginator.entries,
        fn item -> item |> Map.from_struct() |> Map.drop([:__meta__]) end
      )

    Map.merge(
      %{data: data},
      Map.from_struct(paginator.metadata)
    )
  end

  def save_large_file(file_path, content, size \\ 1000) do
    content
    |> Stream.unfold(&String.split_at(&1, size))
    |> Enum.take_while(&(&1 != ""))
    |> Enum.each(fn (chunk) -> File.write!(file_path, chunk, [:append]) end)
  end
end
