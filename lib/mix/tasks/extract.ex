defmodule Mix.Tasks.Extract do
  @moduledoc "The hello mix task: `mix help extract`"
  use Mix.Task
  alias CrossChallenge.Extractor
  alias Ecto.Query
  alias CrossChallenge.{Repo, Info, Utils}

  @shortdoc "Retrieve data from external API."
  def run(_args) do
    Mix.Task.run("app.start")

    Mix.shell().info("=== Extracting data ===")

    {tuple, _, _} =
      System.argv()
      |> OptionParser.parse(
        strict: [
          show_page: :boolean,
          output_file: :string,
          only_output: :boolean,
          failed_pages: :boolean
        ]
      )

    if tuple[:show_page] do
      System.put_env("SHOW_PAGES_NUMBERS", true)
    end

    cond do
      tuple[:only_output] && tuple[:output_file] ->
        save_ouput_in_file(tuple[:output_file])

      tuple[:failed_pages] ->
        output = Extractor.get_failed_pages()
        IO.puts "Has: #{length(output)} failed pages "
        IO.inspect(output)
        if tuple[:output_file], do: save_ouput_in_file(tuple[:output_file], output)

      true ->
        Extractor.run()

        if tuple[:output_file] do
          save_ouput_in_file(tuple[:output_file])
        end
    end
  end

  def save_ouput_in_file(file_path) do
    save_ouput_in_file(file_path, get_data())
  end

  def save_ouput_in_file(file_path, data) do
    with {:ok, json} <- Jason.encode(data) do
      Utils.save_large_file(file_path, json)
    end
  end

  def get_data do
    Info
    |> Query.first()
    |> Repo.one()
    |> Map.from_struct()
    |> Map.take([:all_numbers, :sorted_numbers])
  end
end
