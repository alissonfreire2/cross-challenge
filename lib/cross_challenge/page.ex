defmodule CrossChallenge.Page do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:page, :integer, autogenerate: false, greater_than_or_equal_to: 1}
  @derive {Phoenix.Param, key: :page}
  schema "pages" do
    field :is_failed, :boolean, default: false
    field :last_fetched_at, :utc_datetime
    field :numbers, {:array, :decimal}, default: []
    field :sorted_numbers, {:array, :decimal}, default: []

    timestamps()
  end
  @doc false
  def changeset(page, attrs) do
    page
    |> cast(attrs, [:page, :numbers, :is_failed, :sorted_numbers, :last_fetched_at])
    |> validate_required([:page, :numbers, :is_failed, :sorted_numbers, :last_fetched_at])
    |> validate_number(:page, greater_than_or_equal_to: 1)
  end
end
