defmodule CrossChallenge.Repo.Migrations.AddLastColumnsInInfoTable do
  use Ecto.Migration

  def up do
    alter table(:info) do
      add :last_page, :integer, default: 0
      add :last_stoped_page, :integer, default: 0
    end
  end

  def down do
    alter table(:info) do
      remove :last_page
      remove :last_stoped_page
    end
  end
end
