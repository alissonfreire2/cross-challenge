defmodule CrossChallenge.Extractor do
  alias HTTPoison, as: Http
  alias CrossChallenge.{Repo, Page, Info}
  alias Ecto.{Changeset, Query}
  import Jason, only: [decode: 1]
  import Query, only: [from: 2]

  @attempts_number 3
  @page_limit 100
  def base_url do
    Application.get_env(:cross_challenge, :main)[:numbers_api_url]
  end

  defp parse_url(params) do
    base_url()
    |> URI.parse()
    |> Map.put(:query, URI.encode_query(params))
    |> to_string
  end

  def get_page(page) when is_integer(page) do
    url = parse_url(%{page: page})

    check_numbers = fn map_json ->
      numbers = map_json["numbers"]

      if is_list(numbers) do
        {:ok, numbers}
      else
        {:error, nil}
      end
    end

    if System.get_env("SHOW_PAGES_NUMBERS") do
      IO.puts("page: #{page}")
    end

    result = Http.get(url)

    {:ok, fetched_at} = DateTime.now("Etc/UTC")
    fetched_at = %{fetched_at | microsecond: {0, 0}}

    with {:ok, %Http.Response{body: body}} <- result,
      {:ok, json_map} <- decode(body),
      {:ok, numbers} <- check_numbers.(json_map) do

      {:ok, %{
        "page" => page,
        "fetched_at" => fetched_at,
        "is_failed" => false,
        "numbers" => numbers
      }}
    else
      _ ->
        {:error, %{
            "page" => page,
            "fetched_at" => fetched_at,
            "is_failed" => true,
            "numbers" => []
        }}
    end
  end

  defp reverse_lists(info) do
    %{
      info
      | pages_acc: Enum.reverse(info.pages_acc),
        fetched_pages: Enum.reverse(info.fetched_pages),
        success_pages: Enum.reverse(info.success_pages)
    }
  end

  def get_pages(s, e) do
    get_pages(s, e, %{
      pages_acc: [],
      fetched_pages: [],
      success_pages: [],
      failed_pages: [],
      num_errors: 0,
      last_stoped: 0,
      last_error_page: nil,
      status: :done
    })
  end

  def get_pages(s, e, info) when s <= 0 or e <= 0 do
    info
  end

  def get_pages(s, e, %{num_errors: num_errors, last_error_page: page_error} = info)
    when num_errors >= @attempts_number do
    info = %{info | fetched_pages: [info.fetched_pages | info.last_stoped]}
    if s < e do
      get_pages(s+1, e, %{info | pages_acc: [info.pages_acc | page_error]})
    else
      %{info | status: :maximum_attempts}|> reverse_lists()
    end
  end

  def get_pages(start_page, end_page, info) when start_page == end_page + 1 do
    reverse_lists(info)
  end

  def get_pages(start_page, end_page, info) when start_page <= end_page do
    with {:ok, %{"numbers" => numbers} = page} <- get_page(start_page) do
      if numbers == [] do
        set_info(:ok, info, :finished)
      else
        get_pages(start_page + 1, end_page, set_info(:ok, info, page, start_page))
      end
    else
      {_, error_page} -> get_pages(start_page, end_page, set_info(:error, info, error_page, start_page))
    end
  end

  def get_pages_by_list(%Range{} = list)do
    list
    |> Enum.to_list
    |> get_pages_by_list
  end

  def get_pages_by_list(list) do
    get_pages_by_list(list, %{
      pages_acc: [],
      fetched_pages: [],
      success_pages: [],
      failed_pages: [],
      num_errors: 0,
      last_stoped: 0,
      last_error_page: nil,
      status: :done
    })
  end

  def get_pages_by_list(list, %{num_errors: num_errors, last_error_page: page_error} = info)
    when num_errors >= @attempts_number do
      info = %{info | fetched_pages: [info.fetched_pages | info.last_stoped]}

    if Enum.count(list) > 0 do
      [_|tail] = list
      get_pages_by_list(tail, %{info | pages_acc: [info.pages_acc | page_error]})
    else
      %{info | status: :maximum_attempts} |> reverse_lists()
    end
  end

  def get_pages_by_list([], info) do
    info
  end

  def get_pages_by_list([h | list], info) do
    with {:ok, %{"numbers" => numbers} = page} <- get_page(h) do
      if numbers == [] do
        set_info(:ok, info, :finished)
      else
        get_pages_by_list(list, set_info(:ok, info, page, h))
      end
    else
      {_, error_page} -> get_pages_by_list([h | list], set_info(:error, info, error_page, h))
    end
  end

  defp set_info(:ok, info, page, page_number) do
    %{
      info
      | pages_acc: [page] ++ info.pages_acc,
        fetched_pages: [page_number | info.fetched_pages],
        success_pages: [page_number | info.success_pages],
        num_errors: info.num_errors,
        last_stoped: page_number
    }
  end

  defp set_info(:error, info, error_page, page_number) do
    %{info | num_errors: info.num_errors + 1, last_error_page: error_page, last_stoped: page_number}
  end

  defp set_info(:ok, info, :finished) do
    %{info | status: :finished} |> reverse_lists()
  end

  def get_pages_by_stoped() do
    get_pages_by_stoped(@page_limit)
  end

  def get_pages_by_stoped(limit) do
    info = Info |> Query.first() |> Repo.one()

    last_stoped_page = if info.last_stoped_page == 0, do: 1, else: info.last_stoped_page

    get_pages_by_stoped(limit, last_stoped_page)
  end

  def get_pages_by_stoped(limit, last_stoped_page) do
    get_pages(last_stoped_page, last_stoped_page + limit)
  end

  def run_until_finished(pid) do
    info = get_pages_by_stoped()

    save_pages(info)
    :timer.sleep(500)

    if info.status != :finished do
      get_pages_by_stoped(@page_limit, info.last_stoped_page)
    else
      send(pid, {:finished, info})
      {:ok, info}
    end
  end

  def run do
    Process.flag(:trap_exit, true)
    spawn_link(CrossChallenge.Extractor, :run_until_finished, [self()])

    receive do
      {:finished, info} -> pos_run(info)
      {:EXIT, _pid, _msg} -> run()
      anything -> {:error, anything}
    end
  end

  def pos_run(info) do
    {:ok,
      info,
      insert_all_numbers_in_info()}
  end

  def save_pages(info) do
    Enum.each(info.pages_acc, fn p ->
      map = %{
        page: p["page"],
        numbers: p["numbers"],
        is_failed: p["is_failed"],
        last_fetched_at: p["fetched_at"],
        sorted_numbers: Enum.sort(p["numbers"])
      }

      try do
        struct(Page, map) |> Repo.insert
      rescue
        _ -> Repo.get(Page, p["page"])
          |> Changeset.change(map)
          |> Repo.update!()
      end
    end)

    update_info_table(
      success_pages: info.success_pages,
      failed_pages: info.failed_pages,
      fetched_pages: info.fetched_pages,
      last_stoped_page: info.last_stoped,
      last_page: if(info.status == :finished, do: info.last_stoped, else: 0)
    )
  end

  def get_failed_pages do
    from(Page, where: [is_failed: true], select: [:page])
    |> Repo.all
    |> Enum.map(fn page -> page[:page] end)
  end

  def insert_all_numbers_in_info do
    numbers = from(Page, select: [:numbers])
    |> Repo.all
    |> Enum.map(fn page -> page.numbers end)
    |> List.flatten

    update_info_table(all_numbers: numbers)
  end

  def update_info_table(params) do
    Info
    |> Query.first()
    |> Repo.one()
    |> Changeset.change(params)
    |> Repo.update!()
  end
end
