defmodule CrossChallenge.Repo.Migrations.CreatePages do
  use Ecto.Migration

  def change do
    create table(:pages, primary_key: false) do
      add :page, :integer, primary_key: true
      add :numbers, {:array, :decimal}, default: []
      add :is_failed, :boolean, default: false, null: false
      add :sorted_numbers, {:array, :decimal}, default: []
      add :last_fetched_at, :utc_datetime

      timestamps()
    end

    create index(:pages, [:inserted_at, :page])
  end
end
