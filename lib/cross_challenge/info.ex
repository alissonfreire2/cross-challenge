defmodule CrossChallenge.Info do
  use Ecto.Schema
  import Ecto.Changeset
  import CrossChallenge.Utils

  schema "info" do
    field :all_numbers, {:array, :decimal}
    field :failed_pages, {:array, :integer}
    field :fetched_pages, {:array, :integer}
    field :success_pages, {:array, :integer}
    field :last_page, :integer
    field :last_stoped_page, :integer
    field :last_sorted_at, :utc_datetime
    field :sorted_numbers, {:array, :decimal}

    timestamps()
  end

  @doc false
  def changeset(info, attrs) do
    info
    |> cast(attrs, [
      :success_pages,
      :fetched_pages,
      :failed_pages,
      :last_page,
      :last_stoped_page,
      :all_numbers,
      :sorted_numbers,
      :last_sorted_at
    ])
    |> validate_pages(:success_pages)
    |> validate_pages(:fetched_pages)
    |> validate_pages(:failed_pages)
    |> validate_pages(:last_page)
    |> validate_pages(:last_stoped_page)
    |> validate_last_sorted_at()
  end

  defp validate_pages(changeset, page) do
    changeset
    |> validate_integer(page)
    |> validate_number(page, greater_than_or_equal_to: 0)
  end

  defp validate_last_sorted_at(changeset) do
    last_sorted_at = get_field(changeset, :last_sorted_at)

    cond do
      is_nil(last_sorted_at) or is_datetime?(last_sorted_at) -> changeset
      true -> add_error(changeset, :last_sorted_at, "An datetime is expected.")
    end
  end
end
