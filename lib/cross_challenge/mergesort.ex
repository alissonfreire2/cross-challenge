defmodule CrossChallenge.MergeSort do
  @moduledoc """
    O Merge também utiliza a abordagem dividir para conquistar

    Porém ele vai dividir a lista em duas partes de forma recursiva
    até chegar em um caso base, depois volta realizando o "merge" das duas
    listas
  """
  # Casos base
  def sort([]), do: []
  def sort([i]), do: [i]

  def sort(list) do
    {left, rigth} = split(list)

    merge(sort(left), sort(rigth))
  end

  defp merge(l,r) do
    do_merge(l,r, [])
  end

  defp do_merge(left, [], list) do
    Enum.reverse(list) ++ left
  end

  defp do_merge([], rigth, list) do
    Enum.reverse(list) ++ rigth
  end

  defp do_merge([hl|tl], [hr|_] = rigth, acc) when hl < hr do
    do_merge(tl, rigth, [hl] ++ acc)
  end

  defp do_merge([hl|_] = left, [hr|tr], acc) when hl >= hr do
    do_merge(left, tr, [hr] ++ acc)
  end

  ##### split #####

  def split([]), do: {[], []}
  def split([i]), do: {[i], []}

  def split(list) do
    len = length(list)
    middle = div(len, 2)

    split_in_2(list, {[],[]}, {len, middle, 0})
  end

  defp split_in_2(_, {left, right}, {size, _, counter})
    when counter == size
  do
    { Enum.reverse(left), Enum.reverse(right)}
  end

  defp split_in_2([head | tail], {left, right}, {size, middle, counter})
    when counter < middle
  do
    split_in_2(tail, {[head] ++ left, right}, {size, middle, counter + 1})
  end

  defp split_in_2([head | tail], {left, right}, {size, middle, counter}) do
    split_in_2(tail, {left, [head] ++ right}, {size, middle, counter + 1})
  end
end
