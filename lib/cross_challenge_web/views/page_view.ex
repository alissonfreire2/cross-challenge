defmodule CrossChallengeWeb.PageView do
  use CrossChallengeWeb, :view
  import CrossChallenge.Utils

  @attributes [
    :success_pages,
    :fetched_pages,
    :failed_pages,
    :last_page,
    :last_stoped_page,
    :all_numbers,
    :sorted_numbers,
    :last_sorted_at
  ]

  @page_attributes [
    :page,
    :is_failed,
    :last_fetched_at,
    :numbers,
    :sorted_numbers
  ]

  def render("index.json", %{data: data}) do
    %{
      status: "success",
      data: Map.take(data, @attributes)
    }
  end

  def render("pages.json", %{pages: pages}) do
    %{
      status: "success",
      data: paginator_to_map(pages)
    }
  end

  def render("error.json", error) do
    %{
      status: "fail",
      error: %{
        message: error[:message],
        details: error[:details]
      }
    }
  end

  def render("page.json", %{page: page}) do
    %{
      status: "success",
      data: Map.take(page, @page_attributes)
    }
  end

  def render("numbers.json", %{numbers: numbers}) do
    %{
      status: "success",
      data: %{
        numbers: numbers
      }
    }
  end

  def render("404.json", params) do
    %{
      status: "fail",
      error: %{
        message: params["message"] || "Resource not found",
        details: nil
      }
    }
  end
end
