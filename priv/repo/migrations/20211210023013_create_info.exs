defmodule CrossChallenge.Repo.Migrations.CreateInfo do
  use Ecto.Migration

  def change do
    create table(:info) do
      add :success_pages, :integer, default: 0
      add :fetched_pages, :integer, default: 0
      add :failed_pages, :integer, default: 0
      add :all_numbers, {:array, :decimal}, default: []
      add :sorted_numbers, {:array, :decimal}, default: []
      add :last_sorted_at, :utc_datetime, null: true

      timestamps()
    end
  end
end
