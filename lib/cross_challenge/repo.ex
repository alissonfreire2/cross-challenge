defmodule CrossChallenge.Repo do
  use Ecto.Repo,
    otp_app: :cross_challenge,
    adapter: Ecto.Adapters.Postgres

  require Paginator

  def paginate(queryable, opts \\ [], repo_opts \\ []) do
    try do
      {:ok ,Paginator.paginate(queryable, opts, __MODULE__, repo_opts)}
    rescue
      e -> {:error, e.message}
    end
  end

  def paginate!(queryable, opts \\ [], repo_opts \\ []) do
    Paginator.paginate(queryable, opts, __MODULE__, repo_opts)
  end
end
