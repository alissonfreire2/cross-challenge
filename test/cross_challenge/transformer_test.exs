defmodule CrossChallenge.TransformerTest do
  use CrossChallenge.DataCase
  alias CrossChallenge.Transformer

  test "check if tranformer sort function" do

    unsorted_numbers = case Transformer.get_unsorted_numbers() do
      [] -> for _ <- 0..10, do: :rand.uniform()
      list -> list
    end

    assert Transformer.sort(unsorted_numbers, :merge) == Enum.sort(unsorted_numbers)
    assert Transformer.sort(unsorted_numbers, :quick) == Enum.sort(unsorted_numbers)
  end
end
