defmodule CrossChallengeWeb.PageControllerTest do
  use CrossChallengeWeb.ConnCase
  import CrossChallenge.Utils

  test "GET /api", %{conn: conn} do
    conn = get(conn, "/api")

    assert %{
      "status" => "success",
      "data" => %{
        "success_pages" => success_pages,
        "fetched_pages" => fetched_pages,
        "failed_pages" => failed_pages,
        "last_page" => last_page,
        "last_stoped_page" => last_stoped_page,
        "all_numbers" => all_numbers,
        "sorted_numbers" => sorted_numbers,
        "last_sorted_at" => last_sorted_at
      }
    } = json_response(conn, 200)

    assert is_list(success_pages)
    assert is_list(fetched_pages)
    assert is_list(failed_pages)
    assert is_integer(last_page)
    assert is_integer(last_stoped_page)
    assert is_list(all_numbers)
    assert is_list(sorted_numbers)

    assert is_nil(last_sorted_at) or is_datetime?(last_sorted_at)
  end

  test "GET /api/pages", %{conn: conn} do
    conn = get(conn, "/api/pages")

    assert %{
      "status" => "success",
      "data" => %{
        "data" => data,
        "after" => after_cursor,
        "before" => before_cursor,
        "limit" => limit,
      }
    } = json_response(conn, 200)

    assert is_integer(limit)
    assert is_list(data)

    assert is_nil(after_cursor) or is_bitstring(after_cursor)
    assert is_nil(before_cursor) or is_bitstring(before_cursor)
  end

  test "GET /api/pages?before=ka&after=boom", %{conn: conn} do
    conn = get(conn, "/api/pages?before=ka&after=boom")

    assert %{
      "status" => "fail",
      "error" => %{
        "message" => message,
        "details" => _,
      }
    } = json_response(conn, 400)

    assert is_bitstring(message)
  end

  test "GET /api/pages/1 empty", %{conn: conn} do
    conn = get(conn, "/api/pages/1")

    assert %{
      "status" => "fail",
      "error" => %{
        "message" => message,
        "details" => _,
      }
    } = json_response(conn, 404)

    assert is_bitstring(message)
  end

  test "GET /api/pages/1 with data", %{conn: conn} do
    alias CrossChallenge.{Repo, Page}

    Repo.insert! %Page{page: 1}

    conn = get(conn, "/api/pages/1")

    assert %{
      "status" => "success",
      "data" => %{
        "page" => 1,
        "is_failed" => false,
        "last_fetched_at" => nil,
        "numbers" => [],
        "sorted_numbers" => []
      }
    } = json_response(conn, 200)
  end

  test "GET /api/numbers/ordered", %{conn: conn} do
    conn = get(conn, "/api/numbers/ordered")

    assert %{
      "status" => "success",
      "data" => %{
        "numbers" => ordered_numbers,
      }
    } = json_response(conn, 200)

    assert is_list(ordered_numbers)
  end

  test "GET /api/numbers/unordered", %{conn: conn} do
    conn = get(conn, "/api/numbers/unordered")

    assert %{
      "status" => "success",
      "data" => %{
        "numbers" => unordered_numbers,
      }
    } = json_response(conn, 200)

    assert is_list(unordered_numbers)
  end
end
