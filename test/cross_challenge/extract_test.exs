defmodule CrossChallenge.ExtractorTest do
  use CrossChallenge.DataCase
  alias CrossChallenge.Extractor
  import CrossChallenge.Utils, only: [is_datetime?: 1]

  test "check get_page function" do
    result = Extractor.get_page(1)

    assert {:ok, %{
      "numbers" => numbers,
      "fetched_at" => fetched_at,
      "page" => page,
      "is_failed" => is_failed
    }} = result

    assert page == 1
    assert is_boolean(is_failed)
    assert is_list(numbers)
    assert is_datetime?(fetched_at)
  end

  test "check get_pages function" do
    result = Extractor.get_pages(1, 10)

    assert %{
      pages_acc: pages_acc,
      fetched_pages: fetched_pages,
      success_pages: success_pages,
      failed_pages: failed_pages,
      num_errors: num_errors,
      last_stoped: last_stoped,
      last_error_page: _,
      status: status
    } = result

    assert is_list(pages_acc)
    assert is_list(fetched_pages)
    assert is_list(success_pages)
    assert is_list(failed_pages)
    assert is_integer(num_errors)
    assert is_integer(last_stoped)
    assert is_atom(status)
  end

  test "check get_pages_by_list function" do
    result = Extractor.get_pages_by_list(1..10)

    assert %{
      pages_acc: pages_acc,
      fetched_pages: fetched_pages,
      success_pages: success_pages,
      failed_pages: failed_pages,
      num_errors: num_errors,
      last_stoped: last_stoped,
      last_error_page: last_error_page,
      status: status
    } = result

    assert is_list(pages_acc)
    assert is_list(fetched_pages)
    assert is_list(success_pages)
    assert is_list(failed_pages)
    assert is_integer(num_errors)
    assert is_integer(last_stoped)
    assert is_nil(last_error_page) or is_integer(last_error_page)
    assert is_atom(status)
  end
end
