defmodule CrossChallenge.Transformer do
  alias CrossChallenge.{MergeSort, QuickSort, Repo, Info, Extractor}
  alias Ecto.{Changeset, Query}
  import Query, only: [from: 2]


  def quick(list) do
    QuickSort.sort(list)
  end

  def merge(list) do
    MergeSort.sort(list)
  end

  def standart(list) do
    Enum.sort(list)
  end

  def get_unsorted_numbers do
    from(Info, select: [:all_numbers])
    |> Query.first()
    |> Repo.one()
    |> Map.from_struct
    |> Map.get(:all_numbers)
  end

  def sort(algorithm \\ :merge) do
    list = get_unsorted_numbers()

    sort(list, algorithm)
  end

  def sort(list, algorithm) do
    apply(CrossChallenge.Transformer, algorithm, [list])
  end

  def sort_and_save(algorithm \\ :merge) do
    sort(algorithm) |> save()
  end

  def sort_and_save(list, algorithm) do
    sort(list, algorithm) |> save()
  end

  def save(sorted_numbers) do
    Info
    |> Query.first()
    |> Repo.one()
    |> Changeset.change(
      sorted_numbers: sorted_numbers,
      last_sorted_at: DateTime.utc_now |> DateTime.truncate(:second)
    )|> Repo.update!()
  end

  def extract_transform_and_save(algorithm \\ :merge) do
    with {:ok, _info, all_numbers} <- Extractor.run() do
      sort_and_save(algorithm, all_numbers)
    end
  end
end
