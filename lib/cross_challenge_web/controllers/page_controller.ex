defmodule CrossChallengeWeb.PageController do
  use CrossChallengeWeb, :controller
  import Ecto.Query
  alias CrossChallenge.{Repo, Info, Page}

  action_fallback(CrossChallengeWeb.FallbakController)

  def index(conn, _params) do
    info = Info |> first |> Repo.one()

    render(conn, "index.json", data: info)
  end

  def list_pages(conn, params) do
    query =
      from(p in Page,
        order_by: [
          asc: p.inserted_at,
          desc: p.page
        ],
        select: p
      )

    result =
      Repo.paginate(
        query,
        cursor_fields: [:inserted_at, :page],
        limit: 1,
        after: params["after"],
        before: params["before"]
      )

    case result do
      {:ok, pages} ->
        render(conn, "pages.json", pages: pages)
      {:error, message} ->
        conn
        |> put_status(:bad_request)
        |> render("error.json", message: message)
    end
  end

  def show_page(conn, %{"page" => page_number}) do
    page = Repo.get(Page, page_number)

    case page do
      %Page{} ->
        render(conn, "page.json", page: page)
      nil ->
        conn
        |> put_status(:not_found)
        |> render("404.json")
    end
  end

  def show_numbers(field, conn) do
    numbers = from(i in Info, select: map(i, [field]))
    |> first()
    |> Repo.one()
    |> IO.inspect()
    |> Map.get(field)

      render(conn, "numbers.json", numbers: numbers)
  end

  def show_ordered_numbers(conn, _) do
    show_numbers(:sorted_numbers, conn)
  end

  def show_unordered_numbers(conn, _) do
    show_numbers(:all_numbers, conn)
  end
end
