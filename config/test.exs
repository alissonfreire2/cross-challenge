import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :cross_challenge, CrossChallenge.Repo,
  username: System.get_env("DB_TEST_USER"),
  password: System.get_env("DB_TEST_PASS"),
  database: "#{System.get_env("DB_TEST_DATABASE")}#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: System.get_env("DB_TEST_HOST"),
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :cross_challenge, CrossChallengeWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "N6R0mt9BcbPY+R0jadlNTjws+7fgLgBRkS0hlx7SB0Aml+YF9qUB03yzT74y7Wv8",
  server: false

# In test we don't send emails.
config :cross_challenge, CrossChallenge.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
