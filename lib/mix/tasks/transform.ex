defmodule Mix.Tasks.Transform do
  @moduledoc "The hello mix task: `mix help transform`"
  use Mix.Task
  alias CrossChallenge.Transformer
  alias Ecto.Query
  alias CrossChallenge.{Repo, Info, Utils}

  @shortdoc "Transform data."
  def run(_args) do
    Mix.Task.run("app.start")

    Mix.shell().info("=== Transforming data ===")

    {tuple, _, _} =
      System.argv()
      |> OptionParser.parse(
        strict: [
          sort_and_save: :boolean,
          extract_transform_and_save: :boolean,
          output_file: :string,
          algorithm: :string,
          only_output: :boolean
        ]
      )

      algorithm = get_algorithm(tuple[:algorithm])

      if tuple[:only_output] && tuple[:output_file] do
        save_ouput_in_file(tuple[:output_file])
      else if tuple[:extract_transform_and_save] do
        Transformer.extract_transform_and_save(algorithm)

        if tuple[:output_file] do
          save_ouput_in_file(tuple[:output_file])
        end
      else
        Transformer.sort_and_save(algorithm)

        if tuple[:output_file] do
          save_ouput_in_file(tuple[:output_file])
        end
      end
    end
  end

  def save_ouput_in_file(file_path) do
    with data <- get_data(),
      {:ok, json} <- Jason.encode(data) do
          Utils.save_large_file(file_path, json)
    end
  end

  def get_data do
    Info
    |> Query.first()
    |> Repo.one()
    |> Map.from_struct()
    |> Map.take([:all_numbers, :sorted_numbers])
  end

  defp get_algorithm(algorithm) when is_bitstring(algorithm) do
    algorithm
    |> String.downcase
    |> String.to_atom
    |> get_algorithm()
  end

  defp get_algorithm(:quick), do: :quick
  defp get_algorithm(:standart), do: :standart
  defp get_algorithm(_), do: :merge
  def get_algorithm(), do: :merge
end
