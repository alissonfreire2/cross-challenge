# CrossChallenge

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Copy `.env.example` file to `.env` with `cp .env.example .env`
  * Load enviroment vars with `source .env`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.


Routes:
  *  GET  /api                    
  *  GET  /api/pages              
  *  GET  /api/pages/:page        
  *  GET  /api/numbers/ordered    
  *  GET  /api/numbers/unordered

Mix commands:
  - Extract:
    - `mix extract --output-file=numbers.json`
    - `mix extract --output-file=numbers.json --failed-pages # list failed pages`
    - `mix extract --output-file=numbers.json --only-output`
  - Transform:
    - `mix transform --output-file=numbers.json`
    - `mix transform --output-file=numbers.json --algorithm=merge # defaults`
    - `mix transform --output-file=numbers.json --algorithm=quick`
    - `mix transform --output-file=numbers.json --algorithm=standart # Enum.sort`
    - `mix transform --output-file=numbers.json --only-output`
    - `mix transform --output-file=numbers.json --extract-transform-and-save`

- Before running any command run `source .env` to set the environment variables